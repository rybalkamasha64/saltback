using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using SaltWebApi.Objects;
using SaltWebApi.Repositories;

namespace SaltWebApi.Controllers;

[ApiController]
[EnableCors]
[Route("/standings/")]
public class StandingsController : Controller
{
    private StandingsRepository StandingsRepository;

    public StandingsController()
    {
        StandingsRepository = new StandingsRepository(DBManager.getInstance());
    }

    [HttpGet]
    public List<Standing> Get()
    {
        List<Standing> res = StandingsRepository.GetAllStandings();
        return res;
    }

    [HttpPost]
    public HttpCustomResponse Post(
        float lat,
        float lon,
        string? email,
        string? phone,
        string? name,
        string? image,
        float? raiting
    )
    {
        var result = StandingsRepository.AddStanding(lat, lon, email, phone, name, image, raiting);

        HttpCustomResponse response;
        if (result > 0)
            response = new HttpCustomResponse();
        else if (result < 0)
            response = new HttpCustomResponse("203", "Data not valid");
        else
            response = new HttpCustomResponse("202", "Standing with this params already exist");
        return response;
    }
}