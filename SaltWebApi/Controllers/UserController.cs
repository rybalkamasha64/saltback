using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using SaltWebApi.Objects;
using SaltWebApi.Repositories;

namespace SaltWebApi.Controllers;

[ApiController]
[EnableCors]
[Route("/users/")]
public class UserController : Controller
{
    private UserRepository UserRepository;

    public UserController()
    {
        UserRepository = new UserRepository(DBManager.getInstance());
    }

    [HttpGet]
    public List<User> Get()
    {
        List<User> res = UserRepository.GetAllUsers();
        return res;
    }

    [HttpGet]
    [Route("/users/id/{id}")]
    public User? GetById(int id)
    {
        return UserRepository.GetUser("id", id.ToString());
    }

    [HttpGet]
    [Route("/users/email/{email}")]
    public User? GetByEmail(string email)
    {
        return UserRepository.GetUser("email", email);
    }

    [HttpPost]
    public HttpCustomResponse Post(string email, string password)
    {
        var result = UserRepository.RegisterUser(email, password);

        HttpCustomResponse response;
        if (result > 0)
            response = new HttpCustomResponse();
        else if (result < 0)
            response = new HttpCustomResponse("203", "Data not valid");
        else
            response = new HttpCustomResponse("202", "User with this email already exist");
        return response;
    }

    [HttpDelete]
    [Route("/users/{id}")]
    public HttpCustomResponse Delete(int id)
    {
        var result = UserRepository.DeleteUser(id);
        HttpCustomResponse response;
        if (result > 0)
            response = new HttpCustomResponse();
        else if (result < 0)
            response = new HttpCustomResponse("203", "Data not valid");
        else
            response = new HttpCustomResponse("202", "Nothing was delete");
        return response;
    }

    [HttpPut]
    [Route("/users/{id}")]
    public HttpCustomResponse Put(int id, string? email, string? password, string? name, string? ava)
    {
        var result = UserRepository.UpdateUserData(id, email, password, name, ava);

        HttpCustomResponse response;
        if (result > 0)
            response = new HttpCustomResponse();
        else if (result == 0)
            response = new HttpCustomResponse("202", "Nothing was update");
        else if (result == -1)
            response = new HttpCustomResponse("203", "Data not valid");
        else
            response = new HttpCustomResponse("202", "User with this email already exists");
        return response;
    }
}