using Microsoft.AspNetCore.Mvc;
using SaltWebApi.Objects;
using SaltWebApi.Repositories;
using SaltWebApi.Works;

namespace SaltWebApi.Controllers;

[ApiController]
[Route("/waterarea/")]
public class WaterAreaController : Controller
{
    private WaterAreaRepository WaterAreaRepository;

    public WaterAreaController()
    {
        WaterAreaRepository = new WaterAreaRepository(DBManager.getInstance());
    }

    private double CountLen(Coordinates start, Coordinates finish)
    {
        var R = 6371e3; // metres
        var φ1 = start.Lat * Math.PI / 180; // φ, λ in radians
        var φ2 = finish.Lat * Math.PI / 180;
        var Δφ = (finish.Lat - start.Lat) * Math.PI / 180;
        var Δλ = (finish.Lon - start.Lon) * Math.PI / 180;

        var a = Math.Sin(Δφ / 2) * Math.Sin(Δφ / 2) +
                Math.Cos(φ1) * Math.Cos(φ2) *
                Math.Sin(Δλ / 2) * Math.Sin(Δλ / 2);
        var c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));

        return R * c;
    }

    [HttpGet]
    [Route("/waterarea/")]
    public List<WaterArea> Get()
    {
        return WaterAreaRepository.GetAllWaterArePoints();
    }

    [HttpGet]
    [Route("/waterarea/{startLat},{startLon},{finishLat},{finishLon}")]
    public Way GetWay(float startLat, float startLon, float finishLat, float finishLon)
    {
        Coordinates start = new Coordinates(startLat, startLon);
        Coordinates finish = new Coordinates(finishLat, finishLon);
        FindWay finder = new FindWay();
        // var result = finder.Find(start, finish);
        // if (result.WayDots.Count > 0)
        //     result = finder.Detalize(result);

        Way result = new Way();
        var allGoodVars = finder.FindAll(start, finish);
        double min_len = 7000000.0;
        double straight = CountLen(start, finish);
        int index = -1;
        foreach (var variant in allGoodVars)
        {
            double len = 0.0;

            for (var i = 1; i < variant.WayDots.Count; i++)
            {
                len += CountLen(variant.WayDots[i - 1], variant.WayDots[i]);
            }

            if (len < min_len)
            {
                min_len = len;
                index = allGoodVars.IndexOf(variant);
            }
        }

        if (index >= 0)
            result = allGoodVars[index];

        // result = finder.Detalize(result);
        return result;
    }

    [HttpGet]
    [Route("/waterarea/ways/")]
    public List<Way> GetWays()
    {
        FindWay finder = new FindWay();
        return finder.getAllWays();
    }

    [HttpPost]
    [Route("/waterarea/")]
    public HttpCustomResponse Post(
        int marineId,
        float lat,
        float lon,
        int speed,
        int course,
        int age,
        string dateAdd
    )
    {
        var result = WaterAreaRepository.AddPoint(marineId, lat, lon, speed, course, age, dateAdd);

        HttpCustomResponse response;
        if (result > 0)
            response = new HttpCustomResponse();
        else if (result < 0)
            response = new HttpCustomResponse("203", "Data not valid");
        else
            response = new HttpCustomResponse("202", "Standing with this params already exist");
        return response;
    }
}