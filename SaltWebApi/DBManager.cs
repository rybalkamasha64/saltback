using System.Data.SQLite;

namespace SaltWebApi;

public class DBManager
{
    private static DBManager instance;
    private static SQLiteConnection connection;

    private DBManager()
    {
        connection = CreateConnection();
    }

    public static DBManager getInstance()
    {
        if (instance == null)
            instance = new DBManager();
        return instance;
    }

    public SQLiteConnection getConnection()
    {
        return connection;
    }

    public SQLiteConnection CreateConnection()
    {
        SQLiteConnection sqlite_conn;
        sqlite_conn = new SQLiteConnection("Data Source=salt.db;");

        try
        {
            sqlite_conn.Open();
        }
        catch (Exception ex)
        {
            Console.WriteLine("Error while connection to db");
        }

        return sqlite_conn;
    }

    public void CloseConnection()
    {
        Console.WriteLine("closed");
        connection.Close();
    }

    public void CreateTable(string tableName, string columns)
    {
        SQLiteCommand sqlite_cmd;
        string Createsql = "CREATE TABLE " + tableName + " ( " + columns + " ) ; ";
        sqlite_cmd = connection.CreateCommand();
        sqlite_cmd.CommandText = Createsql;
        sqlite_cmd.ExecuteNonQuery();
    }

    public string ReadData(string tableName)
    {
        SQLiteDataReader sqlite_datareader;
        SQLiteCommand sqlite_cmd;
        connection = CreateConnection();
        sqlite_cmd = connection.CreateCommand();
        sqlite_cmd.CommandText = "SELECT * FROM " + tableName + ";";

        sqlite_datareader = sqlite_cmd.ExecuteReader();
        return ToSplittableString(sqlite_datareader);
    }

    public string ReadDataByField(string tableName, string filedName, string fieldValue)
    {
        SQLiteDataReader sqlite_datareader;
        SQLiteCommand sqlite_cmd;
        connection = CreateConnection();
        sqlite_cmd = connection.CreateCommand();
        sqlite_cmd.CommandText = "SELECT * FROM " + tableName + " WHERE " + filedName + " = \'" + fieldValue + "\' ;";

        sqlite_datareader = sqlite_cmd.ExecuteReader();
        return ToSplittableString(sqlite_datareader);
    }

    // INSERT INTO table1 (column1,column2 ,..) VALUES (value1,value2 ,...), ..., (value1,value2 ,...);
    public int AddData(string tableName, string columns, string values)
    {
        SQLiteCommand sqlite_cmd;
        connection = CreateConnection();
        sqlite_cmd = connection.CreateCommand();
        sqlite_cmd.CommandText = "INSERT INTO " + tableName + " (" + columns + ") VALUES " + "(" + values + ");";
        return sqlite_cmd.ExecuteNonQuery();
    }

    // UPDATE employees SET city = 'Toronto', state = 'ON', postalcode = 'M5P 2N7' WHERE employeeid = 4;
    public int UpdateData(string tableName, int id, string newValues)
    {
        SQLiteCommand sqlite_cmd;
        connection = CreateConnection();
        sqlite_cmd = connection.CreateCommand();
        sqlite_cmd.CommandText = "UPDATE " + tableName + " SET " + newValues + " WHERE id = " + id + ";";

        return sqlite_cmd.ExecuteNonQuery();
    }

    public int DeleteData(string tableName, int id)
    {
        SQLiteCommand sqlite_cmd;
        connection = CreateConnection();
        sqlite_cmd = connection.CreateCommand();
        sqlite_cmd.CommandText = "DELETE FROM " + tableName + " WHERE id = " + id + ";";

        return sqlite_cmd.ExecuteNonQuery();
    }

    public string ToSplittableString(SQLiteDataReader reader)
    {
        string result = "";
        if (reader.HasRows)
            while (reader.Read())
            {
                if (result != "")
                    result += "|";

                var myreader = "";
                for (var i = 0; i < reader.FieldCount; i++)
                {
                    myreader += (i < reader.FieldCount - 1) ? reader.GetValue(i) + "," : reader.GetValue(i);
                }

                result += myreader;
            }

        return result;
    }

    public string ToJSONFormatString(SQLiteDataReader reader)
    {
        string result = "{";
        if (reader.HasRows)
            while (reader.Read())
            {
                if (result != "{") result += ",";

                var myreader = "{";
                for (var i = 0; i < reader.FieldCount; i++)
                {
                    myreader += "\"" + reader.GetName(i) + "\" : \"" + reader.GetValue(i) + "\"";
                    myreader += (i < reader.FieldCount - 1) ? ", " : "} ";
                }

                result += myreader;
            }

        result += "}";

        return result;
    }
}