namespace SaltWebApi.Objects;

public class Acvatory
{
    public Acvatory(int id, Coordinates upLeft, Coordinates downRight)
    {
        Id = id;
        UpLeft = upLeft;
        DownRight = downRight;
    }

    public int Id { get; }
    public Coordinates UpLeft { get; }
    public Coordinates DownRight { get; }
}