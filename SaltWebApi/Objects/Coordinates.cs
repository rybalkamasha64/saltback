namespace SaltWebApi.Objects;

public struct Coordinates
{
    public float Lat { get; }
    public float Lon { get; }

    public Coordinates(float lat, float lon)
    {
        Lat = lat;
        Lon = lon;
    }
}