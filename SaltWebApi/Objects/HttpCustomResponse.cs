namespace SaltWebApi.Objects;

public class HttpCustomResponse
{
    public string Code { get; }
    public string Message { get; }

    public HttpCustomResponse(string code = "200", string message = "OK")
    {
        Code = code;
        Message = message;
    }

    public override string ToString()
    {
        return "{ \"code\" : \"" + Code + "\", \"message\" : \"" + Message + "\"}";
    }
}