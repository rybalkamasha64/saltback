namespace SaltWebApi.Objects;

public class Ship
{
    public int Id { get; }
    public int IdOfCapitan { get; }
    public int Length { get; }

    public Ship(int id, int length, int idOfCapitan)
    {
        Id = id;
        Length = length;
        IdOfCapitan = idOfCapitan;
    }
}