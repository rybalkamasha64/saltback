using System.Net;

namespace SaltWebApi.Objects;

public class Standing
{
    private const string STANDINGS_URL_GET =
        "https://maps.googleapis.com/maps/api/place/textsearch/json?input=стоянки%20катеров%20приморский%20край&inputtype=textquery&key=AIzaSyCrkuu9yvz1aT6Wi08uHJMSqC5wXFUW1uE";

    public int Id { get; }
    public Coordinates Coordinates { get; }
    public string? Email { get; }
    public string? Phone { get; }
    public string? Image { get; }
    public string? Name { get; }
    public float Raiting { get; }

    public Standing(int id, Coordinates coordinates, string? email, string? phone, string? image, string? name,
        float raiting)
    {
        Id = id;
        Email = email;
        Phone = phone;
        Image = image;
        Name = name;
        Coordinates = coordinates;
        Raiting = raiting;
    }

    public Standing(string[] data)
    {
        Id = Int32.Parse(data[0]);
        Coordinates = new Coordinates(float.Parse(data[1]), float.Parse(data[2]));
        Email = data[3];
        Phone = data[4];
        Name = data[5];
        Image = data[6];
        Raiting = (data[7] != "") ? float.Parse(data[7]) : 0;
    }

    public void GetStandings()
    {
        using WebClient wc = new WebClient();
        var json = wc.DownloadString(STANDINGS_URL_GET);
        byte[] bytes = wc.DownloadData(
            "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photo_reference=Aap_uECZzGmP3akTY8j3_4i89bi17uLVx7nUU7_7PZR68h4E7R0RE0kS86VkKZ8eLGxkzYS09b8Dl0dJGiyyVPKnABVJyEICKxvz0v7BkJNJsK0pVDKhk8OWNiGg8d0qWDgIC1loQauocxZtXKYENdq4xgsBDVGeK_8Leyw02HQ34oi_AJyk&key=AIzaSyCrkuu9yvz1aT6Wi08uHJMSqC5wXFUW1uE");
        var converted = Convert.ToBase64String(bytes);
    }
}