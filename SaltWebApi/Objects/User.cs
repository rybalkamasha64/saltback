namespace SaltWebApi.Objects;

public class User
{
    public int Id { get; }
    public string Email { get; }
    public string? Name { get; }
    public string? Ava { get; }
    public string Password { get; }

    public User(string email, string password, string? name, string? ava, int id = 0)
    {
        Id = id;
        Email = email;
        Password = password;
        Name = name;
        Ava = ava;
    }

    public User(string[] data)
    {
        Id = Int32.Parse(data[0]);
        Email = data[1];
        Password = data[2];
        Name = data[3];
        Ava = data[4];
    }
}