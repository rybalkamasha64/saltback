namespace SaltWebApi.Objects;

public class WaterArea
{
    public int Id { get; }
    public int MarineId { get; }
    public Coordinates Coordinates { get; }
    public int Speed { get; }
    public int Course { get; }
    public int Age { get; }
    public string DateAdd { get; }

    public WaterArea(int id, int speed, int course,
        int age, string dateAdd, int marineId, Coordinates coordinates)
    {
        Id = id;
        Coordinates = coordinates;
        Speed = speed;
        Course = course;
        Age = age;
        DateAdd = dateAdd;
        MarineId = marineId;
    }

    public WaterArea(string[] data)
    {
        Id = Int32.Parse(data[0]);
        MarineId = Int32.Parse(data[1]);
        Coordinates = new Coordinates(float.Parse(data[2]), float.Parse(data[3]));
        Speed = Int32.Parse(data[4]);
        Course = Int32.Parse(data[5]);
        Age = Int32.Parse(data[6]);
        DateAdd = data[7];
    }
}