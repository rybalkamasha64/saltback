namespace SaltWebApi.Objects;

public class Way
{
    public int? Id { get; set; }
    public int? WaterAreaId { get; set; }
    public Coordinates? From { get; set; }
    public Coordinates? To { get; set; }
    public List<Coordinates> WayDots { get; set; }
    public List<Coordinates>? Smooth { get; set; }

    public Way()
    {
        Id = null;
        WayDots = new List<Coordinates>();
    }

    public Way(int id, List<Coordinates> wayDots, Coordinates? from, Coordinates? to, int? waterAreaId)
    {
        Id = id;
        From = from;
        WayDots = wayDots;
        To = to;
        WaterAreaId = waterAreaId;
    }
}