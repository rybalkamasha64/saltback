using SaltWebApi.Works;

namespace SaltWebApi
{
    internal static class Program
    {
        static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            builder.Services.AddRouting(r => r.SuppressCheckForUnhandledSecurityMetadata = true);
            builder.Services.AddControllers();
            builder.Services.AddSwaggerGen(c => { c.SwaggerDoc("v1", new() {Title = "SaltWebApi", Version = "v1"}); });
            builder.Services.AddExpoClient();

            var app = builder.Build();

            DBManager dbManager = DBManager.getInstance();
            dbManager.CreateConnection();

            var updater = Updater.getInstance();
            updater.UpdateData();

            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "SaltWebApi v1"));
            }

            app.UseHttpsRedirection();
            app.UseAuthorization();
            app.UseRouting();
            app.UseCors(options => { options.AllowAnyOrigin().AllowAnyMethod(); });

            app.MapControllers();
            app.Run();

            dbManager.CloseConnection();
        }
    }
}