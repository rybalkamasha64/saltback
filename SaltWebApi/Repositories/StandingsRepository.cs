using SaltWebApi.Objects;

namespace SaltWebApi.Repositories;

public class StandingsRepository
{
    private DBManager DBManager;

    public StandingsRepository(DBManager dbManager)
    {
        DBManager = dbManager;
    }

    public List<Standing> GetAllStandings()
    {
        var result = DBManager.ReadData("Standings");
        var standings = new List<Standing>();

        if (result != "")
        {
            var arrOfStandings = result.Split("|");
            foreach (var standing in arrOfStandings)
            {
                var splStanding = standing.Split(",");
                var newStanding = new Standing(splStanding);
                standings.Add(newStanding);
            }
        }

        return standings;
    }

    public int AddStanding(
        float lat,
        float lon,
        string? email,
        string? phone,
        string? name,
        string? image,
        float? raiting
    )
    {
        if (lat == 0 || lon == 0) return -1;

        var columns = "lat, lon";
        if (email != null) columns += ", email";
        if (phone != null) columns += ", phone";
        if (name != null) columns += ", name";
        if (image != null) columns += ", image";
        if (raiting != null) columns += ", raiting";

        var values = "'" + lat + "', '" + lon;
        if (email != null) values += "', '" + email;
        if (phone != null) values += "', '" + phone;
        if (name != null) values += "', '" + name;
        if (image != null) values += "', '" + image;
        if (raiting != null) values += "', '" + raiting;
        values += "'";

        return DBManager.AddData("Standings", columns, values);
    }
}