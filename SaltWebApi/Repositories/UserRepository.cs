using SaltWebApi.Objects;

namespace SaltWebApi.Repositories;

public class UserRepository
{
    private DBManager DBManager;

    public UserRepository(DBManager dbManager)
    {
        DBManager = dbManager;
    }

    public List<User> GetAllUsers()
    {
        var result = DBManager.ReadData("Users");
        var users = new List<User>();

        if (result != "")
        {
            var arrOfUsers = result.Split("|");
            foreach (var user in arrOfUsers)
            {
                var splUser = user.Split(",");
                var newUser = new User(splUser);
                users.Add(newUser);
            }
        }

        return users;
    }

    public User? GetUser(string filed, string value)
    {
        var result = DBManager.ReadDataByField("Users", filed, value).Split(",");
        return (result != null && result.Length > 0 && result[0] != "") ? new User(result) : null;
    }

    public int RegisterUser(string email, string password)
    {
        if (email == "" || password == "") return -1;

        var check = DBManager.ReadDataByField("Users", "email", email);
        if (check != "") return 0;

        var columns = "email, password";
        var values = "'" + email + "', '" + password + "'";

        return DBManager.AddData("Users", columns, values);
    }

    public int UpdateUserData(int id, string? email, string? password, string? name, string? ava)
    {
        if (email == "" || password == "" || (email == null && password == null && name == null)) return -1;
        string values = "";
        if (email != null)
        {
            var check = DBManager.ReadDataByField("Users", "email", email);
            if (check != "") return -2;

            values += "email = '" + email + "'";
        }

        if (password != null)
            values += (values == "") ? "password = '" + password + "'" : ", " + "password = '" + password + "'";

        if (name != null)
            values += (values == "") ? "name = '" + name + "'" : ", " + "name = '" + name + "'";

        if (ava != null)
            values += (values == "") ? "ava = '" + ava + "'" : ", " + "ava = '" + ava + "'";


        return DBManager.UpdateData("Users", id, values);
    }

    public int DeleteUser(int id)
    {
        return DBManager.DeleteData("Users", id);
    }
}