using SaltWebApi.Objects;

namespace SaltWebApi.Repositories;

public class WaterAreaRepository
{
    private DBManager DBManager;

    public WaterAreaRepository(DBManager dbManager)
    {
        DBManager = dbManager;
    }

    public List<WaterArea> GetAllWaterArePoints()
    {
        var result = DBManager.ReadData("WaterAreaClustered");
        var standings = new List<WaterArea>();

        if (result != "")
        {
            var arrOfStandings = result.Split("|");
            foreach (var standing in arrOfStandings)
            {
                var splStanding = standing.Split(",");
                var newStanding = new WaterArea(splStanding);
                standings.Add(newStanding);
            }
        }

        return standings;
    }


    public int AddPoint(
        int marineId,
        float lat,
        float lon,
        int speed,
        int course,
        int age,
        string dateAdd
    )
    {
        if (lat == 0 || lon == 0) return -1;

        var columns = "id_marine, lat, lon, speed, course, age, date_add";

        var values =
            "'" + marineId +
            "', '" + lat +
            "', '" + lon +
            "', '" + speed +
            "', '" + course +
            "', '" + age +
            "', '" + dateAdd + "'";

        return DBManager.AddData("WaterAreaFiltered", columns, values);
    }
}