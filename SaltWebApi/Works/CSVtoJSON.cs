namespace SaltWebApi.Works;

class WayUnit
{
    public float Lat { get; set; }
    public float Lon { get; set; }
    public int Speed { get; set; }
    public int Course { get; set; }
    public int Age { get; set; }
    public string DateAdd { get; set; }

    public WayUnit(float lat, float lon, int speed, int course, int age, string dateAdd)
    {
        Lat = lat;
        Lon = lon;
        Speed = speed;
        Course = course;
        Age = age;
        DateAdd = dateAdd;
    }
}

public class CSVtoJSON
{
    private static CSVtoJSON _instance;
    private List<string> smallShipsIds;

    private CSVtoJSON()
    {
        smallShipsIds = new List<string>();
    }

    public static CSVtoJSON GetInstance()
    {
        if (_instance == null)
            _instance = new CSVtoJSON();
        return _instance;
    }

    public void ConvertShips()
    {
        var lines = File.ReadAllLines(@"Sources/targets.csv");
        var result = "";
        using StreamWriter writer = new StreamWriter(@"Sources/targets.json");
        using StreamWriter smallWriter = new StreamWriter(@"Sources/small_targets.json");
        writer.Write("{\n   \"ships\" : [\n");
        smallWriter.Write("{\n   \"ships\" : [\n");
        for (int i = 1; i < lines.Length; i++)
        {
            string line = lines[i].Replace("\"", "");
            var newArrOfLine = line.Split(';');
            var newJson = "    {\n" +
                          "     \"id\" : \"" + newArrOfLine[0] + "\", \n" +
                          "     \"length\" : \"" + newArrOfLine[6] + "\" \n   }";
            if (!result.Contains(newJson) && newArrOfLine[6] != "0")
            {
                if (i < lines.Length - 1) newJson += ',';
                result += newJson;
                writer.Write(newJson);
                if (Int32.Parse(newArrOfLine[6]) <= 20)
                {
                    smallShipsIds.Add(newArrOfLine[0]);
                    smallWriter.Write(newJson);
                }
            }
        }

        writer.Write("]\n}");
        smallWriter.Write("]\n}");
    }

    public void ConvertWays()
    {
        Dictionary<string, List<WayUnit>> shipsData = new Dictionary<string, List<WayUnit>>();

        using (FileStream fs = File.Open(@"Sources/25.10.2015.csv", FileMode.Open))
        using (BufferedStream bs = new BufferedStream(fs))
        using (StreamReader sr = new StreamReader(bs))
        {
            string? s;
            while ((s = sr.ReadLine()) != null)
            {
                var newPosition = s.Replace("\"", "").Replace(",", ".").Split(';');

                if (newPosition[2] != "lat" && (Int32.Parse(newPosition[4]) == 0 || Int32.Parse(newPosition[5]) != 511))
                {
                    if (shipsData.ContainsKey(newPosition[1]))
                    {
                        int indexOfSameDate = -1;
                        int numberOfDots = shipsData[newPosition[1]].Count;

                        for (var j = 0; j < numberOfDots; j++)
                        {
                            if (shipsData[newPosition[1]][j].DateAdd == newPosition[7]) indexOfSameDate = j;
                        }

                        if (indexOfSameDate >= 0)
                        {
                            var shipWayPoint = shipsData[newPosition[1]][indexOfSameDate];
                            if (shipWayPoint.Age > Int32.Parse(newPosition[6]))
                            {
                                shipWayPoint.Lon = float.Parse(newPosition[2]);
                                shipWayPoint.Lat = float.Parse(newPosition[3]);
                                shipWayPoint.Speed = Int32.Parse(newPosition[4]);
                                shipWayPoint.Course = Int32.Parse(newPosition[5]);
                                shipWayPoint.Age = Int32.Parse(newPosition[6]);
                            }
                        }
                        else
                        {
                            if (
                                shipsData[newPosition[1]][numberOfDots - 1].Lat > (float.Parse(newPosition[3]) + 0.01)
                                ||
                                shipsData[newPosition[1]][numberOfDots - 1].Lat < (float.Parse(newPosition[3]) - 0.01)
                                ||
                                shipsData[newPosition[1]][numberOfDots - 1].Lon > (float.Parse(newPosition[2]) + 0.002)
                                ||
                                shipsData[newPosition[1]][numberOfDots - 1].Lon < (float.Parse(newPosition[2]) - 0.002)
                                ||
                                shipsData[newPosition[1]][numberOfDots - 1].Speed != 0
                            )
                            {
                                WayUnit newPoint = new WayUnit(
                                    float.Parse(newPosition[3]),
                                    float.Parse(newPosition[2]),
                                    Int32.Parse(newPosition[4]),
                                    Int32.Parse(newPosition[5]),
                                    Int32.Parse(newPosition[6]),
                                    newPosition[7]
                                );
                                shipsData[newPosition[1]].Add(newPoint);
                            }
                        }
                    }
                    else
                    {
                        WayUnit newPoint = new WayUnit(
                            float.Parse(newPosition[3]),
                            float.Parse(newPosition[2]),
                            Int32.Parse(newPosition[4]),
                            Int32.Parse(newPosition[5]),
                            Int32.Parse(newPosition[6]),
                            newPosition[7]
                        );
                        shipsData.Add(newPosition[1], new List<WayUnit>());
                        shipsData[newPosition[1]].Add(newPoint);
                    }
                }
            }
        }

        ConvertToJson(shipsData);
    }

    private void ConvertToJson(Dictionary<string, List<WayUnit>> shipsData)
    {
        using (StreamWriter writer = new StreamWriter(@"Sources/water_area.json"))
        using (StreamWriter smallWriter = new StreamWriter(@"Sources/small_water_area.json"))
        {
            writer.WriteLine("{\n  \"ways\" : [");
            smallWriter.WriteLine("{\n  \"ways\" : [");
            int counter = 1;
            foreach (var ship in shipsData)
            {
                writer.WriteLine("  {\n    \"ship_id\" : \"" + ship.Key + "\",");
                writer.WriteLine("    \"ship_points\" : [");

                if (smallShipsIds.Contains(ship.Key))
                {
                    smallWriter.WriteLine("  {\n    \"ship_id\" : \"" + ship.Key + "\",");
                    smallWriter.WriteLine("    \"ship_points\" : [");
                }

                for (var i = 0; i < ship.Value.Count; i++)
                {
                    writer.WriteLine("    {");
                    writer.WriteLine("      \"Lat\" : \"" + ship.Value[i].Lat + "\",");
                    writer.WriteLine("      \"Lon\" : \"" + ship.Value[i].Lon + "\",");
                    writer.WriteLine("      \"Speed\" : \"" + ship.Value[i].Speed + "\",");
                    writer.WriteLine("      \"Course\" : \"" + ship.Value[i].Course + "\",");
                    writer.WriteLine("      \"Age\" : \"" + ship.Value[i].Age + "\",");
                    writer.WriteLine("      \"DateAdd\" : \"" + ship.Value[i].DateAdd + "\"");
                    writer.WriteLine("    }");
                    writer.WriteLine((i < ship.Value.Count - 1) ? "," : "");

                    if (smallShipsIds.Contains(ship.Key))
                    {
                        smallWriter.WriteLine("    {");
                        smallWriter.WriteLine("      \"Lat\" : \"" + ship.Value[i].Lat + "\",");
                        smallWriter.WriteLine("      \"Lon\" : \"" + ship.Value[i].Lon + "\",");
                        smallWriter.WriteLine("      \"Speed\" : \"" + ship.Value[i].Speed + "\",");
                        smallWriter.WriteLine("      \"Course\" : \"" + ship.Value[i].Course + "\",");
                        smallWriter.WriteLine("      \"Age\" : \"" + ship.Value[i].Age + "\",");
                        smallWriter.WriteLine("      \"DateAdd\" : \"" + ship.Value[i].DateAdd + "\"");
                        smallWriter.WriteLine("    }");
                        smallWriter.WriteLine((i < ship.Value.Count - 1) ? "," : "");
                    }
                }

                writer.Write("    ]\n  }");
                writer.WriteLine((counter < shipsData.Count) ? "," : "");

                if (smallShipsIds.Contains(ship.Key))
                {
                    smallWriter.Write("    ]\n  }");
                    smallWriter.WriteLine((counter < shipsData.Count) ? "," : "");
                }

                counter++;
            }

            writer.Write("  ]\n}");
            smallWriter.Write("  ]\n}");
        }
    }
}