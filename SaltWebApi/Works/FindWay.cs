using Aglomera;
using Aglomera.D3;
using Aglomera.Linkage;
using Newtonsoft.Json;
using SaltWebApi.Objects;
using SaltWebApi.Repositories;

namespace SaltWebApi.Works;

class DataPoint : IComparable<DataPoint>
{
    public string id { get; set; }
    public double x { get; set; }
    public double y { get; set; }

    public DataPoint(string id, double x, double y)
    {
        this.id = id;
        this.x = x;
        this.y = y;
    }

    public int CompareTo(DataPoint other)
    {
        if (this.x < other.x)
            return -1;
        if (this.x < other.x + 1 && this.x < other.x - 1)
            return 0;
        return 1;
    }
}

class DissimilarityMetric : IDissimilarityMetric<DataPoint>
{
    public double Calculate(DataPoint instance1, DataPoint instance2)
    {
        return Math.Sqrt((instance2.x - instance1.x) * (instance2.x - instance1.x) +
                         (instance2.y - instance1.y) * (instance2.y - instance1.y));
    }
}

public class FindWay
{
    private const double RADIUS = 0.05;
    private List<Way> ways;

    public List<Way> getAllWays()
    {
        List<int> ships = new List<int>();
        List<Way> result = new List<Way>();
        WaterAreaRepository WaterAreaRepository = new WaterAreaRepository(DBManager.getInstance());

        List<WaterArea> data = WaterAreaRepository.GetAllWaterArePoints();

        List<Coordinates> wayDots = new List<Coordinates>();
        Way newWay = new Way();

        foreach (var record in data)
        {
            if (!ships.Contains(record.MarineId))
            {
                if (newWay.Id != null)
                {
                    newWay.WayDots = wayDots;
                    if (wayDots.Count > 2)
                    {
                        result.Add(newWay);
                    }

                    wayDots = new List<Coordinates> {record.Coordinates};
                    newWay = new Way(record.Id, wayDots, null, null, null);
                }
                else
                {
                    newWay.Id = 0;
                    wayDots.Add(record.Coordinates);
                }

                ships.Add(record.MarineId);
            }
            else
            {
                wayDots.Add(record.Coordinates);
            }
        }

        if (wayDots.Count > 2)
        {
            result.Add(newWay);
        }

        List<Way> newResult = new List<Way>();
        foreach (var way in result)
        {
            newResult.Add(way);
        }

        return newResult;
    }

    public static async void CheckIfWater(float lat, float lon)
    {
        string baseUrl = "https://api.onwater.io/api/v1/results/"; //[latitude],[longitude]
        string token = "?access_token=ptYvCW3dzrUiUxRdr5w3";

        try
        {
            using (HttpClient client = new HttpClient())
            {
                string str = baseUrl + lat + "," + lon + token;
                using (HttpResponseMessage res = await client.GetAsync(str))
                {
                    using (HttpContent content = res.Content)
                    {
                        Console.WriteLine(content.Headers);
                    }
                }
            }
        }
        catch (Exception exception)
        {
            Console.WriteLine("Exception Hit------------");
            Console.WriteLine(exception);
        }
    }

    public void Hier()
    {
        var points = getAllWays();
        var dataPoints = new HashSet<DataPoint>();
        List<Coordinates> startPoints = new List<Coordinates>();
        var k = 0;
        DissimilarityMetric metric;
        AverageLinkage<DataPoint> linkage;
        AgglomerativeClusteringAlgorithm<DataPoint> algorithm;
        foreach (var point in points)
        {
            if (startPoints.Contains(point.WayDots[0]))
            {
                foreach (var dot in point.WayDots)
                {
                    dataPoints.Add(new DataPoint(k.ToString(), dot.Lat, dot.Lon));
                }
            }
            else
            {
                if (k != 0)
                {
                    metric = new DissimilarityMetric();
                    linkage = new AverageLinkage<DataPoint>(metric);
                    algorithm = new AgglomerativeClusteringAlgorithm<DataPoint>(linkage);
                    var clusteringResult = algorithm.GetClustering(dataPoints);
                    clusteringResult.SaveD3DendrogramFile(@"Sources/test" + k + ".json",
                        formatting: Formatting.Indented);
                }

                dataPoints = new HashSet<DataPoint>();
                foreach (var dot in point.WayDots)
                {
                    dataPoints.Add(new DataPoint(k.ToString(), dot.Lat, dot.Lon));
                }

                startPoints.Add(point.WayDots[0]);
            }

            k++;
        }

        metric = new DissimilarityMetric();
        linkage = new AverageLinkage<DataPoint>(metric);
        algorithm = new AgglomerativeClusteringAlgorithm<DataPoint>(linkage);
        var clusteringResultNew = algorithm.GetClustering(dataPoints);
        clusteringResultNew.SaveD3DendrogramFile(@"Sources/test" + k + ".json", formatting: Formatting.Indented);
    }

    public void HierForAll()
    {
        var points = getAllWays();
        var dataPoints = new HashSet<DataPoint>();
        var k = 0;
        DissimilarityMetric metric;
        AverageLinkage<DataPoint> linkage;
        AgglomerativeClusteringAlgorithm<DataPoint> algorithm;
        foreach (var point in points)
        {
            // foreach (var dot in point.WayDots)
            // {
            dataPoints.Add(new DataPoint(k.ToString(), point.WayDots[^1].Lat, point.WayDots[^1].Lon));
            // }
        }

        metric = new DissimilarityMetric();
        linkage = new AverageLinkage<DataPoint>(metric);
        algorithm = new AgglomerativeClusteringAlgorithm<DataPoint>(linkage);
        var clusteringResultNew = algorithm.GetClustering(dataPoints);
        clusteringResultNew.SaveD3DendrogramFile(@"Sources/testStarts.json", formatting: Formatting.Indented);
    }

    private void SaveToLog()
    {
    }

    private List<Coordinates> TranslateToList(double[,] start)
    {
        List<Coordinates> res = new List<Coordinates>();

        for (int j = 0; j < start.Length / 2; j++)
        {
            res.Add(new Coordinates(float.Parse(start[0, j].ToString()), float.Parse(start[1, j].ToString())));
        }

        return res;
    }

    private List<double[]> TranslateToArr(List<Coordinates> start)
    {
        List<double[]> res = new List<double[]>();
        var x = new double[start.Count];
        var y = new double[start.Count];
        for (int j = 0; j < start.Count; j++)
        {
            x[j] = start[j].Lat;
            y[j] = start[j].Lon;
        }

        res.Add(x);
        res.Add(y);
        return res;
    }

    private bool InCircle(Coordinates center, Coordinates dot, double radius = RADIUS)
    {
        return (Math.Sqrt((dot.Lat - center.Lat) * (dot.Lat - center.Lat) +
                          (dot.Lon - center.Lon) * (dot.Lon - center.Lon)) < radius * radius);
    }

    public Way Find(Coordinates start, Coordinates finish, bool isSec = false)
    {
        List<Way> ways = getAllWays();
        List<Way> from = new List<Way>();
        List<Way> to = new List<Way>();
        Way result = new Way();
        foreach (var way in ways)
        {
            var wasAddedToFrom = false;
            if (InCircle(start, way.WayDots[0]) && InCircle(finish, way.WayDots[^1]))
            {
                return way;
            }

            if (InCircle(start, way.WayDots[0]))
            {
                from.Add(way);
                wasAddedToFrom = true;
            }

            if (InCircle(finish, way.WayDots[^1]))
            {
                to.Add(way);
                if (wasAddedToFrom)
                    return way;
            }
        }

        if (to.Count == 0 && from.Count != 0)
            foreach (var startSameWay in from)
            {
                double minLen = 0.02;
                int minInd = -1;
                for (var i = 1; i < startSameWay.WayDots.Count; i++)
                {
                    double dist = Math.Sqrt(
                        (startSameWay.WayDots[i].Lat - finish.Lat) * (startSameWay.WayDots[i].Lat - finish.Lat) +
                        (startSameWay.WayDots[i].Lon - finish.Lon) * (startSameWay.WayDots[i].Lon - finish.Lon));
                    if (dist < minLen)
                    {
                        minLen = dist;
                        minInd = i;
                    }
                }

                if (minInd > 0 && minLen < 0.02)
                {
                    result.WayDots.AddRange(startSameWay.WayDots.GetRange(0, minInd + 1));
                    if (!InCircle(finish, startSameWay.WayDots[minInd]) && !isSec)
                    {
                        var found = Find(startSameWay.WayDots[minInd], finish, true);
                        result.WayDots.AddRange(found.WayDots);
                    }

                    return result;
                }
            }
        else if (from.Count == 0 && to.Count != 0)
            foreach (var finishSameWay in to)
            {
                double minLen = 0.02;
                int minInd = -1;
                for (var i = 0; i < finishSameWay.WayDots.Count - 1; i++)
                {
                    double dist = Math.Sqrt(
                        (finishSameWay.WayDots[i].Lat - start.Lat) * (finishSameWay.WayDots[i].Lat - start.Lat) +
                        (finishSameWay.WayDots[i].Lon - start.Lon) * (finishSameWay.WayDots[i].Lon - start.Lon));
                    if (dist < minLen)
                    {
                        minLen = dist;
                        minInd = i;
                    }
                }

                if (minInd > 0 && minLen < 0.02)
                {
                    result.WayDots.AddRange(
                        finishSameWay.WayDots.GetRange(minInd, finishSameWay.WayDots.Count - minInd));
                    if (!InCircle(start, finishSameWay.WayDots[minInd]) && !isSec)
                    {
                        var found = Find(start, finishSameWay.WayDots[minInd], true);
                        result.WayDots.AddRange(found.WayDots);
                    }

                    return result;
                }
            }
        else if (from.Count != 0 && to.Count != 0)
            foreach (var startSameWay in from)
            {
                if (startSameWay.WayDots.Contains(finish))
                {
                    var ind = startSameWay.WayDots.IndexOf(finish);

                    result.WayDots.AddRange(startSameWay.WayDots.GetRange(0, ind + 1));
                    return result;
                }

                for (var i = 1; i < startSameWay.WayDots.Count; i++)
                {
                    foreach (var endSameWay in to)
                    {
                        if (endSameWay.WayDots.Contains(startSameWay.WayDots[i]))
                        {
                            var firstPartWay = startSameWay.WayDots.GetRange(0, i + 1);
                            var indTo = endSameWay.WayDots.IndexOf(startSameWay.WayDots[i]);
                            var secPartWay = endSameWay.WayDots.GetRange(indTo, endSameWay.WayDots.Count - indTo);
                            result.WayDots.AddRange(firstPartWay);
                            result.WayDots.AddRange(secPartWay);
                            return result;
                        }
                    }
                }
            }
        else if (!isSec)
            result = Find(finish, start, true);

        return result;
    }

    public List<Way> FindAll(Coordinates start, Coordinates finish, bool isSec = false)
    {
        List<Way> ways = getAllWays();
        List<Way> from = new List<Way>();
        List<Way> to = new List<Way>();

        List<Way> allGoodVars = new List<Way>();

        foreach (var way in ways)
        {
            var wasAddedToFrom = false;
            if (InCircle(start, way.WayDots[0]) && InCircle(finish, way.WayDots[^1]))
            {
                allGoodVars.Add(way);
                return allGoodVars;
            }

            if (InCircle(start, way.WayDots[0]))
            {
                from.Add(way);
                wasAddedToFrom = true;
            }

            if (InCircle(finish, way.WayDots[^1]))
            {
                to.Add(way);
                if (wasAddedToFrom)
                {
                    allGoodVars.Add(way);
                    return allGoodVars;
                }
            }
        }

        if (to.Count == 0 && from.Count != 0)
            foreach (var startSameWay in from)
            {
                double minLen = 0.02;
                int minInd = -1;
                for (var i = 1; i < startSameWay.WayDots.Count; i++)
                {
                    double dist = Math.Sqrt(
                        (startSameWay.WayDots[i].Lat - finish.Lat) * (startSameWay.WayDots[i].Lat - finish.Lat) +
                        (startSameWay.WayDots[i].Lon - finish.Lon) * (startSameWay.WayDots[i].Lon - finish.Lon));
                    if (dist < minLen)
                    {
                        minLen = dist;
                        minInd = i;
                    }
                }

                if (minInd > 0 && minLen < 0.02)
                {
                    Way variant = new Way();
                    variant.WayDots.AddRange(startSameWay.WayDots.GetRange(0, minInd + 1));
                    if (!InCircle(finish, startSameWay.WayDots[minInd]) && !isSec)
                    {
                        var found = Find(startSameWay.WayDots[minInd], finish, true);
                        variant.WayDots.AddRange(found.WayDots);
                    }

                    allGoodVars.Add(variant);
                    // return allGoodVars;
                }
            }
        else if (from.Count == 0 && to.Count != 0)
            foreach (var finishSameWay in to)
            {
                double minLen = 0.02;
                int minInd = -1;
                for (var i = 0; i < finishSameWay.WayDots.Count - 1; i++)
                {
                    double dist = Math.Sqrt(
                        (finishSameWay.WayDots[i].Lat - start.Lat) * (finishSameWay.WayDots[i].Lat - start.Lat) +
                        (finishSameWay.WayDots[i].Lon - start.Lon) * (finishSameWay.WayDots[i].Lon - start.Lon));
                    if (dist < minLen)
                    {
                        minLen = dist;
                        minInd = i;
                    }
                }

                if (minInd > 0 && minLen < 0.02)
                {
                    Way variant = new Way();
                    variant.WayDots.AddRange(
                        finishSameWay.WayDots.GetRange(minInd, finishSameWay.WayDots.Count - minInd));
                    if (!InCircle(start, finishSameWay.WayDots[minInd]) && !isSec)
                    {
                        var found = Find(start, finishSameWay.WayDots[minInd], true);
                        variant.WayDots.AddRange(found.WayDots);
                    }

                    allGoodVars.Add(variant);
                    // return allGoodVars;
                }
            }
        else if (from.Count != 0 && to.Count != 0)
            foreach (var startSameWay in from)
            {
                if (startSameWay.WayDots.Contains(finish))
                {
                    var ind = startSameWay.WayDots.IndexOf(finish);

                    Way variant = new Way();
                    variant.WayDots = startSameWay.WayDots.GetRange(0, ind + 1);
                    allGoodVars.Add(variant);
                    return allGoodVars;
                }
                else
                    for (var i = 1; i < startSameWay.WayDots.Count; i++)
                    {
                        foreach (var endSameWay in to)
                        {
                            if (endSameWay.WayDots.Contains(startSameWay.WayDots[i]))
                            {
                                var firstPartWay = startSameWay.WayDots.GetRange(0, i + 1);
                                var indTo = endSameWay.WayDots.IndexOf(startSameWay.WayDots[i]);
                                var secPartWay = endSameWay.WayDots.GetRange(indTo, endSameWay.WayDots.Count - indTo);

                                Way variant = new Way();
                                variant.WayDots.AddRange(firstPartWay);
                                variant.WayDots.AddRange(secPartWay);
                                allGoodVars.Add(variant);
                                // return allGoodVars;
                            }
                        }
                    }
            }

        if (!isSec)
            allGoodVars.AddRange(FindAll(finish, start, true));

        return allGoodVars;
    }

    public Way Detalize(Way src)
    {
        Way res = new Way();
        for (var i = 1; i < src.WayDots.Count; i++)
        {
            var prevDot = src.WayDots[i - 1];
            var curDot = src.WayDots[i];

            var midDotLat = (prevDot.Lat + curDot.Lat) / 2;
            var midDotLon = (prevDot.Lon + curDot.Lon) / 2;

            var midDot = new Coordinates(midDotLat, midDotLon);

            res.WayDots.Add(prevDot);
            res.WayDots.Add(midDot);
        }

        res.WayDots.Add(src.WayDots[^1]);
        return res;
    }
}