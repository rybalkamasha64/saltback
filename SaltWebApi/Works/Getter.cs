using System.Net;
using SaltWebApi.Repositories;

namespace SaltWebApi.Works;

public class Getter
{
    private const string STANDINGS_URL_GET =
        "https://maps.googleapis.com/maps/api/place/textsearch/json?input=стоянки%20катеров%20приморский%20край&inputtype=textquery&key=AIzaSyCrkuu9yvz1aT6Wi08uHJMSqC5wXFUW1uE";

    private static readonly string[] namesOfFields = new string[]
    {
        "lat",
        "lng",
        "name",
        "photo_reference",
        "rating",
        "formatted_phone_number",
        "email",
        "place_id",
    };

    private static Getter _instance;

    private Getter()
    {
    }

    public static Getter GetInstance()
    {
        if (_instance == null)
            _instance = new Getter();
        return _instance;
    }

    private Dictionary<string, string> stringValidate(string standing, WebClient wc)
    {
        var fields = standing.Split("\n");
        bool reading = false;
        string res = "";
        Dictionary<string, string> dic = new Dictionary<string, string>
        {
            {"lat", null},
            {"lng", null},
            {"email", null},
            {"formatted_phone_number", null},
            {"name", null},
            {"photo_reference", null},
            {"rating", null},
            {"place_id", null},
        };
        foreach (var field in fields)
        {
            if (field != "")
            {
                var keyValue = field.Replace("\"", "").Replace(",", "").Replace("'", "").Split(":");
                if (keyValue.Length == 2 && keyValue[1].Trim(' ') != "{" &&
                    namesOfFields.Contains(keyValue[0].Trim(' ')) && dic[keyValue[0].Trim(' ')] == null)
                    dic[keyValue[0].Trim(' ')] = keyValue[1].Trim(' ');
            }
        }

        if (dic["formatted_phone_number"] == null)
        {
            string responce = wc.DownloadString(
                String.Format(
                    "https://maps.googleapis.com/maps/api/place/details/json?place_id={0}&fields=formatted_phone_number&key=AIzaSyCrkuu9yvz1aT6Wi08uHJMSqC5wXFUW1uE",
                    dic["place_id"]));
            var number = responce.Replace("\"", "").Split('\n')[3].Split(':');
            if (number.Length == 2 && number[0].Trim(' ') != "status")
                dic["formatted_phone_number"] = number[1].Trim(' ');
        }

        if (dic["photo_reference"] != null)
        {
            byte[] bytes = wc.DownloadData(
                String.Format(
                    "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photo_reference={0}&key=AIzaSyCrkuu9yvz1aT6Wi08uHJMSqC5wXFUW1uE",
                    dic["photo_reference"]));
            var converted = Convert.ToBase64String(bytes);
            dic["photo_reference"] = converted;
        }

        return dic;
    }

    public void GetStandings()
    {
        using WebClient wc = new WebClient();
        var json = wc.DownloadString(STANDINGS_URL_GET);
        if (json != "")
        {
            {
                using StreamWriter writer = new StreamWriter(@"Sources/standings.json");
                writer.WriteLine(json);
            }

            var lines = json.Split('\n');
            string[] valuableLines = new string[lines.Length - 7];
            Array.Copy(lines, 3, valuableLines, 0, lines.Length - 7);

            lines = String.Join("\n", valuableLines).Split("},\n      {");
            lines[0] = lines[0].Trim(' ').Trim('{');
            lines[^1] = lines[^1].Trim('}');

            StandingsRepository standingsRepository = new StandingsRepository(DBManager.getInstance());

            foreach (var line in lines)
            {
                var valide = stringValidate(line, wc);
                standingsRepository.AddStanding(
                    float.Parse(valide["lat"]),
                    float.Parse(valide["lng"]),
                    valide["email"],
                    valide["formatted_phone_number"],
                    valide["name"],
                    valide["photo_reference"],
                    float.Parse(valide["rating"]));
            }
        }
    }

    public void GetWaterArea()
    {
        var json = File.ReadAllLines(@"Sources/water_area.json");
        string[] valuableLines = new string[json.Length - 8];
        Array.Copy(json, 3, valuableLines, 0, json.Length - 8);
        var text = String.Join('\n', valuableLines);
        json = text.Split("},\n    {");
        if (json.Length > 1)
        {
            WaterAreaRepository waterAreaRepository = new WaterAreaRepository(DBManager.getInstance());

            foreach (var data in json)
            {
                var mappedData = data.Trim(' ').Trim('\n').Split('\n');
                string shipId = mappedData[0].Replace("\"", "").Replace(",", "").Split(':')[1];
                string[] cleanDataOfOneShip = new string[mappedData.Length - 5];
                Array.Copy(mappedData, 3, cleanDataOfOneShip, 0, mappedData.Length - 5);

                cleanDataOfOneShip = String.Join('\n', cleanDataOfOneShip).Split("},\n        {");
                foreach (var point in cleanDataOfOneShip)
                {
                    var mappedPoint = point
                        .Trim(' ')
                        .Replace("\"", "")
                        .Replace("\n", "").Split(",");

                    List<string> res = new List<string> { };
                    res.Add(shipId);
                    foreach (var pointData in mappedPoint)
                    {
                        var KeyValuePoint = pointData.Trim(' ').Split(':');
                        if (KeyValuePoint.Length == 2)
                            res.Add(KeyValuePoint[1] ?? "");
                        else if (KeyValuePoint.Length > 2)
                        {
                            string[] date = new string[KeyValuePoint.Length - 1];
                            Array.Copy(KeyValuePoint, 1, date, 0, KeyValuePoint.Length - 1);
                            res.Add(String.Join(":", date));
                        }
                    }

                    if (res.Count == 7)
                        waterAreaRepository.AddPoint(
                            Int32.Parse(res[0].TrimStart(' ')),
                            float.Parse(res[1].TrimStart(' ')),
                            float.Parse(res[2].TrimStart(' ')),
                            Int32.Parse(res[3].TrimStart(' ')),
                            Int32.Parse(res[4].TrimStart(' ')),
                            Int32.Parse(res[5].TrimStart(' ')),
                            res[6]
                        );
                }
            }
        }
    }

    public void GetShips()
    {
        var json = File.ReadAllLines(@"Sources/targets.json");
        string[] valuableLines = new string[json.Length - 8];
        Array.Copy(json, 3, valuableLines, 0, json.Length - 8);
        var text = String.Join('\n', valuableLines);
        json = text.Split("},\n    {");
        if (json.Length > 1)
        {
        }
    }
}