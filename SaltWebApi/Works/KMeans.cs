using SaltWebApi.Objects;

namespace SaltWebApi.Works;

public class KMeans
{
    static int[] InitClustering(int numTuples,
        int numClusters, int randomSeed)
    {
        Random random = new Random(randomSeed);
        int[] clustering = new int[numTuples];
        for (int i = 0; i < numClusters; ++i)
            clustering[i] = i;
        for (int i = numClusters; i < clustering.Length; ++i)
            clustering[i] = random.Next(0, numClusters);
        return clustering;
    }

    static int[] Cluster(double[][] rawData, int numClusters,
        int numAttributes, int maxCount)
    {
        bool changed = true;
        int ct = 0;
        int numTuples = rawData.Length;
        int[] clustering = InitClustering(numTuples, numClusters, 0);
        double[][] means = Allocate(numClusters, numAttributes);
        double[][] centroids = Allocate(numClusters, numAttributes);
        UpdateMeans(rawData, clustering, means);
        UpdateCentroids(rawData, clustering, means, centroids);
        while (changed == true && ct < maxCount)
        {
            ++ct;
            changed = Assign(rawData, clustering, centroids);
            UpdateMeans(rawData, clustering, means);
            UpdateCentroids(rawData, clustering, means, centroids);
        }

        return clustering;
    }

    static int MinIndex(double[] distances)
    {
        int indexOfMin = 0;
        double smallDist = distances[0];
        for (int k = 0; k < distances.Length; ++k)
        {
            if (distances[k] < smallDist)
            {
                smallDist = distances[k];
                indexOfMin = k;
            }
        }

        return indexOfMin;
    }

    static bool Assign(double[][] rawData,
        int[] clustering, double[][] centroids)
    {
        int numClusters = centroids.Length;
        bool changed = false;
        double[] distances = new double[numClusters];
        for (int i = 0; i < rawData.Length; ++i)
        {
            for (int k = 0; k < numClusters; ++k)
                distances[k] = Distance(rawData[i], centroids[k]);
            int newCluster = MinIndex(distances);
            if (newCluster != clustering[i])
            {
                changed = true;
                clustering[i] = newCluster;
            }
        }

        return changed;
    }

    static double Distance(double[] tuple, double[] vector)
    {
        double sumSquaredDiffs = 0.0;
        for (int j = 0; j < tuple.Length; ++j)
            sumSquaredDiffs += Math.Pow((tuple[j] - vector[j]), 2);
        return Math.Sqrt(sumSquaredDiffs);
    }

    static void UpdateCentroids(double[][] rawData, int[] clustering,
        double[][] means, double[][] centroids)
    {
        for (int k = 0; k < centroids.Length; ++k)
        {
            double[] centroid = ComputeCentroid(rawData, clustering, k, means);
            centroids[k] = centroid;
        }
    }

    static double[] ComputeCentroid(double[][] rawData, int[] clustering,
        int cluster, double[][] means)
    {
        int numAttributes = means[0].Length;
        double[] centroid = new double[numAttributes];
        double minDist = double.MaxValue;
        for (int i = 0; i < rawData.Length; ++i) // Перебираем каждую последовательность данных
        {
            int c = clustering[i];
            if (c != cluster) continue;
            double currDist = Distance(rawData[i], means[cluster]);
            if (currDist < minDist)
            {
                minDist = currDist;
                for (int j = 0; j < centroid.Length; ++j)
                    centroid[j] = rawData[i][j];
            }
        }

        return centroid;
    }

    static double[][] Allocate(int numClusters, int numAttributes)
    {
        double[][] result = new double[numClusters][];
        for (int k = 0; k < numClusters; ++k)
            result[k] = new double[numAttributes];
        return result;
    }

    static void UpdateMeans(double[][] rawData, int[] clustering,
        double[][] means)
    {
        int numClusters = means.Length;
        for (int k = 0; k < means.Length; ++k)
        for (int j = 0; j < means[k].Length; ++j)
            means[k][j] = 0.0;
        int[] clusterCounts = new int[numClusters];
        for (int i = 0; i < rawData.Length; ++i)
        {
            int cluster = clustering[i];
            ++clusterCounts[cluster];
            for (int j = 0; j < rawData[i].Length; ++j)
                means[cluster][j] += rawData[i][j];
        }

        for (int k = 0; k < means.Length; ++k)
        for (int j = 0; j < means[k].Length; ++j)
            means[k][j] /= clusterCounts[k]; // опасность
        return;
    }

    static void ShowVector(int[] vector)
    {
        for (int i = 0; i < vector.Length; ++i)
            Console.Write(vector[i] + " ");
        Console.WriteLine("");
    }

    static void ShowVector(double[] vector)
    {
        for (int i = 0; i < vector.Length; ++i)
            Console.Write(vector[i].ToString() + " ");
        Console.WriteLine("");
    }

    static void ShowClustering(double[][] rawData,
        int numClusters, int[] clustering)
    {
        for (int k = 0; k < numClusters; ++k) // Каждый кластер
        {
            for (int i = 0; i < rawData.Length; ++i) // Каждая последовательность
                if (clustering[i] == k)
                {
                    for (int j = 0; j < rawData[i].Length; ++j)
                        Console.Write(rawData[i][j].ToString() + " ");
                    Console.WriteLine("");
                }

            Console.WriteLine("");
        }
    }

    static void WriteClustering(double[][] rawData, int numClusters, int[] clustering)
    {
        using StreamWriter writer = new StreamWriter(@"Sources/kmeansResults.json");
        writer.WriteLine("[");
        for (int k = 0; k < numClusters; ++k)
        {
            writer.WriteLine("[");
            for (int i = 0; i < rawData.Length; ++i)
                if (clustering[i] == k)
                {
                    writer.WriteLine("{");
                    writer.WriteLine("\"latitude\" : " + rawData[i][0].ToString() + ",");
                    writer.WriteLine("\"longitude\" : " + rawData[i][1].ToString());
                    writer.WriteLine("},");
                }

            writer.WriteLine("],");
        }

        writer.WriteLine("]");
    }

    static void ShowMatrix(double[][] matrix)
    {
        for (int i = 0; i < matrix.Length; ++i)
        {
            Console.Write("[" + i.ToString().PadLeft(2) + "]  ");
            for (int j = 0; j < matrix[i].Length; ++j)
                Console.Write(matrix[i][j].ToString() + "  ");
            Console.WriteLine("");
        }
    }

    public Way Calculate()
    {
        FindWay finder = new FindWay();
        var ways = finder.getAllWays();

        try
        {
            string[] attributes = new string[] {"Height", "Weight"};
            int k = 0;
            int maxNumDots = 0;
            foreach (var way in ways)
            {
                maxNumDots += way.WayDots.Count;
            }

            double[][] rawData = new double[maxNumDots][];
            foreach (var way in ways)
            {
                foreach (var dot in way.WayDots)
                {
                    rawData[k] = new double[] {dot.Lat, dot.Lon};
                    k++;
                }
            }

            int numAttributes = attributes.Length;
            int numClusters = 4;
            int maxCount = 70;
            int[] clustering = Cluster(rawData, numClusters, numAttributes, maxCount);
            WriteClustering(rawData, numClusters, clustering);
            Console.WriteLine("\nClustering complete");
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }

        return new Way();
    }

    public int[] GetPreGetClusters()
    {
        // Dictionary<int, int[]> clusters = new Dictionary<int, int[]>
        // {
        //     {0, new int[] {23}},
        //     {0, new int[] {4, 10, 16, 29}},
        //     {0, new int[] {7}},
        //     {0, new int[] {11, 18, 30}},
        //     {0, new int[] {14}},
        //     {0, new int[] {13}},
        //     {0, new int[] {25}},
        //     {0, new int[] {27}},
        //     {0, new int[] {32}},
        //     {0, new int[] {34}},
        //     {0, new int[] {9}},
        //     {0, new int[] {5}},
        // };
        // [23], [2], [4, 10, 16, 29], [7], [11, 18, 30], [14], [13], [25], [27], [32], [34], [9], [5]
        //198, 415, 487, 1642, 1794, 1837, 2002, 2253, 2297, 2509, 2772, 3883, 4523, 5119, 5585, 5683, 5905, 6173

        return new int[]
        {
            144, 187, 50, 160, 247, 134, 75, 153, 44, 118, 244, 161, 274, 168, 120, 2, 277, 100
        };
    }
}