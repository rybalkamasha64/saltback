namespace SaltWebApi.Works;

public class Updater
{
    private static Updater instance;

    private Updater()
    {
    }

    public static Updater getInstance()
    {
        if (instance == null)
            instance = new Updater();
        return instance;
    }

    public void UpdateData()
    {
        string nextPlanUpdate = "";

        try
        {
            var lines = File.ReadAllLines(@"Sources/lastUpdate.txt");
            nextPlanUpdate = (lines[0] == "12")
                ? (Int32.Parse(lines[1]) + 1).ToString() + "1"
                : lines[1] + (Int32.Parse(lines[0]) + 1).ToString();
        }
        catch
        {
            Console.WriteLine("File not exist");
        }

        if (String.Compare(nextPlanUpdate, DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString()) < 0)
        {
            Console.WriteLine("Updating...");

            // var translater = CSVtoJSON.GetInstance();
            // translater.ConvertShips();
            // translater.ConvertWays();
            //
            // Getter standing = Getter.GetInstance();
            // standing.GetStandings();
            // standing.GetWaterArea();
            //
            // using StreamWriter writer = new StreamWriter(@"Sources/lastUpdate.txt");
            // writer.WriteLine(DateTime.Now.Month.ToString());
            // writer.WriteLine(DateTime.Now.Year.ToString());

            // Getter standing = Getter.GetInstance();
            // standing.GetStandings();
            // standing.GetWaterArea();
            // standing.GetShips();

            Console.WriteLine("Updated!");
        }
    }
}